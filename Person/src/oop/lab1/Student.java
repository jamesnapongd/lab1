package oop.lab1;

//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	/**
	 * Compare student's by name.
	 * They are equal if the name matches.
	 * @param other is another Object to compare to this one.
	 * @return true if the name is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if(obj.getClass() != this.getClass() ){
			return false;
		}
		Person other = (Person) obj;
		if(this.getName().equalsIgnoreCase(other.getName())){
			return true;
		}
		return false;
	}
}
